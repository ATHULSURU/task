import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';
import { IEMPLOYEE } from '../employee';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  public employee: IEMPLOYEE = { title: '', description: ''};
  constructor(private route: Router, private _employeeService: EmployeeService) { }

  ngOnInit() {
  }
  createEmployee() {

    this._employeeService.createEmployee(this.employee)
        .subscribe( () => {
          this.route.navigate(['/employee-list']);
        });
}
}
