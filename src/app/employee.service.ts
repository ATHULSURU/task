import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IEMPLOYEE } from './employee';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private httpOptions = { headers: this.headers};
  private _url = 'http://localhost:8080/api/books/';
  constructor(private http: HttpClient) { }

  getEmployees(): Observable<IEMPLOYEE[]> {
    return this.http.get<IEMPLOYEE[]>(this._url).pipe(
          catchError(this.errorHandler));
  }
  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Server-Error');

  }
  createEmployee(employee): Observable<IEMPLOYEE> {
    console.log(JSON.stringify(employee));
    return this.http.post<IEMPLOYEE>(this._url, JSON.stringify(employee), this.httpOptions);
  }
   deleteEmployee(employee) {
    return this.http.delete(this._url + '/' + employee.id);
  }

  getEmploye(index): Observable<IEMPLOYEE> {
    return this.http.get<IEMPLOYEE>(this._url + '/' + index, this.httpOptions );
  }
  saveemployee(data, index) {
    return this.http.put<IEMPLOYEE>(this._url + index, JSON.stringify(data),
    this.httpOptions);
  }
}
