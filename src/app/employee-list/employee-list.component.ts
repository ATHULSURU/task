import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  public employees = [];
  public i;

  public errorMsg;
  constructor(private _employeeService: EmployeeService, private route: Router) { }

  ngOnInit() {
    this._employeeService.getEmployees()
    .subscribe(data => this.employees = data);  }

      deleteEmployee(employee): void {
      this._employeeService.deleteEmployee(employee).subscribe( data => {
          this.employees = this.employees.filter(u => u !== employee  );
        });
    }
    onSelect(id) {
        this.route.navigate(['/employee-dtls', id]);
    }
    toUpdate(id) {
      this.route.navigate(['/employee-edit', id]);
    }
}
