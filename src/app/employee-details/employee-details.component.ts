import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  public employees = [];
  public index;
  public oneEmployee;
  public errorMsg;

  constructor(private _employeeService: EmployeeService,
     private route: ActivatedRoute) {
      }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('id'), 10);
      this._employeeService.getEmploye(this.index).subscribe(data => {
        this.oneEmployee = data;
      });
    });

    this._employeeService.getEmployees()
    .subscribe(data => this.employees = data,
               error => this.errorMsg = error);
               console.log(JSON.stringify(this.employees));
  }

}
