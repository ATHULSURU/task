import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/employee.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { IEMPLOYEE } from 'src/app/employee';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }
  public task: IEMPLOYEE;
  public index;

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('id'), 10);
      this.employeeService.getEmploye(this.index).subscribe(data => {
        this.task = data;
      });
    });
  }
    saveChanges(data) {
      this.employeeService.saveemployee(data, this.index).subscribe(() => {
        this.router.navigate(['/employee-list']);
      });
    }
}
