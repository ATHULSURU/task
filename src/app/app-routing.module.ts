import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { UpdateComponent } from './employee-details/update/update.component';

const routes: Routes = [
  {path: 'employee-dtls/:id', component: EmployeeDetailsComponent},
  {path: 'add-employee', component: AddEmployeeComponent},
  {path: 'employee-list', component: EmployeeListComponent},
  {path: 'employee-edit/:id' , component: UpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
